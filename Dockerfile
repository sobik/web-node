FROM ubuntu:xenial
# Convert build arguments to env variables since build arguemnts does not exist upon container start
ARG JENKINS_HOST
ENV JENKINS_HOST=$JENKINS_HOST
ARG JENKINS_JNLP_SECRET
ENV JENKINS_JNLP_SECRET=$JENKINS_JNLP_SECRET
ARG JENKINS_WORK_DIR
ENV JENKINS_WORK_DIR=$JENKINS_WORK_DIR

WORKDIR /root
# Install Git and dependencies
RUN dpkg --add-architecture i386 \
 && apt-get update \
 && apt-get install -y file git curl zip libncurses5:i386 libstdc++6:i386 zlib1g:i386 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists /var/cache/apt

# Get actual agent.jar
RUN curl $JENKINS_HOST/jnlpJars/agent.jar -o agent.jar

RUN apt-get update
RUN apt-get install -y software-properties-common python-software-properties docker curl libfontconfig
RUN curl -sSL https://get.docker.com/ | sh
RUN mkdir -p /root/.docker/
RUN echo '{"auths": {"https://registry.budgetbakers.com:5000": {}, "registry.budgetbakers.com:5000": {"auth": "YWRtaW46YWRtaW5XYWxsZXQyMDE3Lg=="}},"HttpHeaders": {"User-Agent": "Docker-Client/18.05.0-ce (linux)"}}' >>/root/.docker/config.json
RUN add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
RUN apt-get update
RUN echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
RUN yes | apt-get install -y --allow-unauthenticated oracle-java8-installer oracle-java8-set-default
RUN apt-get install -y lib32stdc++6 lib32z1 protobuf-compiler apt-transport-https

RUN touch /root/ADDED_VARS

#Node
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs python-dev file

#Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get -y update && apt-get -y install yarn

# Sentry
RUN curl -sL https://sentry.io/get-cli/ | bash

#Gauco for rancher API
RUN mkdir -p /opt/gauco && cd /opt/gauco && git clone https://github.com/BudgetBakers/gaucho.git /opt/gauco && chmod +x /opt/gauco/services.py
RUN apt-get install --fix-missing -y python-pip nano && pip install baker websocket requests

#Python3
ADD requirements.txt /root/requirements.txt
RUN apt-get install -y python3 python3-pip
RUN pip3 install -r requirements.txt

CMD java -jar agent.jar -jnlpUrl $JENKINS_HOST/computer/web-node/slave-agent.jnlp -secret $JENKINS_JNLP_SECRET -workDir $JENKINS_WORK_DIR]
